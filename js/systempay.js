/**
 * @file
 * Provides an autosubmit for the payment form : when the transaction
 * is started, it will autosubmit in 3 seconds. The user has a fallback
 * button "If the redirection is not working, click here" if it fails.
 */

(function () {
  'use strict';

  setInterval(function () {
    document.forms['systempay-init-form'].submit();
  }, 3000);

}());
