<?php

declare(strict_types = 1);

namespace Drupal\systempay\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\payment\Entity\PaymentInterface;
use Drupal\payment\Payment;
use Drupal\systempay\Libs\SystempayHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Form to init Systempay.
 */
class SystempayInitForm extends FormBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * InitForm constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   Current user service.
   */
  public function __construct(AccountProxyInterface $currentUser) {
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'systempay_init_form';
  }

  /**
   * Custom access provider.
   *
   * It checks:
   * - User has permission to do payments with Systempay.
   * - Payment status is pending.
   * - Payer is current user.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   The current payment entity.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   True if authorized.
   */
  public function access(PaymentInterface $payment = NULL) {
    if (!$this->currentUser->hasPermission('execute systempay payment')) {
      return AccessResult::forbidden('Doesn\'t have permission to execute Systempay payment.');
    }

    if ((int) $payment->getOwnerId() !== $this->currentUser->id()) {
      return AccessResult::forbidden('This payment doesn\'t belong to you.');
    }

    if ($payment->getPaymentStatus()->getPluginId() !== 'payment_pending') {
      return AccessResult::forbidden('Status doesn\'t match expected.');
    }

    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?PaymentInterface $payment = NULL) {
    if (!$payment) {
      throw new HttpException(404);
    }

    // Instanciate the class with the config in the constructor.
    $systempayHelper = new SystempayHelper($payment->getPaymentMethod()->getPluginDefinition());

    // Check amount is valid.
    if ($payment->getAmount() <= 0) {
      $paymentStatus = Payment::statusManager()->createInstance($systempayHelper->getStatusIdFromCode('30'));
      $payment->setPaymentStatus($paymentStatus);
      $payment->save();
      // We can throw a 500, the payment type should normally handle that case.
      throw new HttpException(500, 'Payment amount invalid, expected ≥ 0.');
    }

    // Make the array using the helper class.
    $payment_data = $systempayHelper->createVadsArray($payment);

    return $systempayHelper->createFormFromArray($payment_data);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // This is not supposed to happened.
    throw new HttpException(403, 'Forbidden');
  }

}
