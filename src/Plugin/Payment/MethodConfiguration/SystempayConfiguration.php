<?php

declare(strict_types = 1);

namespace Drupal\systempay\Plugin\Payment\MethodConfiguration;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\payment\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationBase;
use Drupal\plugin\Plugin\Plugin\PluginSelector\PluginSelectorManagerInterface;
use Drupal\plugin\PluginType\PluginTypeInterface;
use Drupal\systempay\Libs\SystempayHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The form for Systempay method configuration.
 *
 * @PaymentMethodConfiguration(
 *     description=@Translation("Pay with SystemPay."),
 *     id="systempay_payment",
 *     label=@Translation("Systempay")
 * )
 */
class SystempayConfiguration extends PaymentMethodConfigurationBase implements ContainerFactoryPluginInterface {

  /**
   * The payment status plugin type.
   *
   * @var \Drupal\plugin\PluginType\PluginTypeInterface
   */
  protected $paymentStatusType;

  /**
   * The plugin selector manager.
   *
   * @var \Drupal\plugin\Plugin\Plugin\PluginSelector\PluginSelectorManagerInterface
   */
  protected $pluginSelectorManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, TranslationInterface $string_translation, ModuleHandlerInterface $module_handler, PluginSelectorManagerInterface $plugin_selector_manager, PluginTypeInterface $payment_status_type) {
    $configuration += $this->defaultConfiguration();

    parent::__construct($configuration, $plugin_id, $plugin_definition, $string_translation, $module_handler);
    $this->paymentStatusType = $payment_status_type;
    $this->pluginSelectorManager = $plugin_selector_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $systempayHelper = new SystempayHelper($this->configuration);

    // ==========================> Fieldset : Payment Gateway Access.
    $form['systempay_module_param'] = [
      '#type' => 'fieldset',
      '#title' => $this->stringTranslation->translate('Payment gateway access'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['systempay_module_param']['systempay_site_id'] = [
      '#type' => 'textfield',
      '#size' => '20',
      '#title' => $this->stringTranslation->translate('Site ID'),
      '#description' => $this->stringTranslation->translate('The identifier provided by your bank'),
      '#default_value' => $this->configuration['systempay_site_id'] ?? '12345678',
      '#required' => TRUE,
    ];
    $form['systempay_module_param']['systempay_key_test'] = [
      '#type' => 'textfield',
      '#size' => '30',
      '#title' => $this->stringTranslation->translate('Certificate in test mode'),
      '#description' => $this->stringTranslation->translate('Certificate provided by your bank for test (Available on the back-office Systempay)'),
      '#default_value' => $this->configuration['systempay_key_test'] ?? '1111111111111111',
      '#required' => TRUE,
    ];
    $form['systempay_module_param']['systempay_key_prod'] = [
      '#type' => 'textfield',
      '#size' => '30',
      '#title' => $this->stringTranslation->translate('Certificate in production mode'),
      '#description' => $this->stringTranslation->translate('Certificate provided by your bank (Available on the back-office Systempay after validation)'),
      '#default_value' => $this->configuration['systempay_key_prod'] ?? '2222222222222222',
      '#required' => TRUE,
    ];
    $form['systempay_module_param']['systempay_ctx_mode'] = [
      '#type' => 'radios',
      '#title' => $this->stringTranslation->translate('Mode'),
      '#description' => $this->stringTranslation->translate('The context mode of this module'),
      '#options' => [
        'TEST' => $this->stringTranslation->translate('TEST'),
        'PRODUCTION' => $this->stringTranslation->translate('PRODUCTION'),
      ],
      '#default_value' => $this->configuration['systempay_ctx_mode'] ?? 'TEST',
      '#required' => TRUE,
    ];
    $form['systempay_module_param']['systempay_platform_url'] = [
      '#type' => 'textfield',
      '#title' => $this->stringTranslation->translate('Platform URL'),
      '#description' => $this->stringTranslation->translate('Link to the payment platform'),
      '#default_value' => $this->configuration['systempay_platform_url'] ?? 'https://paiement.systempay.fr/vads-payment/',
      '#required' => TRUE,
    ];
    $form['systempay_module_param']['systempay_value_min'] = [
      '#type' => 'number',
      '#min' => '0',
      '#step' => '1',
      '#title' => $this->stringTranslation->translate('Minimum authorized value'),
      '#description' => $this->stringTranslation->translate('The minimum value for paiement. Write it in centimes (ex : for 30€, write 3000).'),
      '#default_value' => $this->configuration['systempay_value_min'] ?? '2000',
      '#required' => TRUE,
    ];
    $form['systempay_module_param']['systempay_value_max'] = [
      '#type' => 'number',
      '#min' => '0',
      '#step' => '1',
      '#title' => $this->stringTranslation->translate('Maximum authorized value'),
      '#description' => $this->stringTranslation->translate('The maximum value for paiement. Write it in centimes (ex : for 30€, write 3000). Write 0 for none.'),
      '#default_value' => $this->configuration['systempay_value_max'] ?? '0',
      '#required' => TRUE,
    ];

    // ==========================> Fieldset : Payment Page.
    $form['systempay_payment_page'] = [
      '#type' => 'fieldset',
      '#title' => $this->stringTranslation->translate('Payment page'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['systempay_payment_page']['systempay_capture_delay'] = [
      '#type' => 'textfield',
      '#size' => '20',
      '#title' => $this->stringTranslation->translate('Capture delay'),
      '#description' => $this->stringTranslation->translate('The number of days before the bank restoration (adjustable in your back-office Systempay)'),
      '#default_value' => $this->configuration['systempay_capture_delay'] ?? '0',
    ];
    $form['systempay_payment_page']['systempay_validation_mode'] = [
      '#type' => 'select',
      '#title' => $this->stringTranslation->translate('Validation mode'),
      '#description' => $this->stringTranslation->translate('If manual is selected, you will have to confirm payments manually in your back-office Systempay'),
      '#options' => $systempayHelper->getAvailableValidationModes(),
      '#default_value' => $this->configuration['systempay_validation_mode'] ?? '0',
    ];
    $form['systempay_payment_page']['systempay_payment_cards'] = [
      '#type' => 'select',
      '#multiple' => 'multiple',
      '#title' => $this->stringTranslation->translate('Card Types'),
      '#description' => $this->stringTranslation->translate('The card type(s) that can be used for the payment'),
      '#options' => $systempayHelper->getSupportedCardTypes(),
      '#default_value' => $this->configuration['systempay_payment_cards'] ?? '',
    ];

    // ==========================> Fieldset : URLs to Shops.
    $form['systempay_urls_to_shop'] = [
      '#type' => 'fieldset',
      '#title' => $this->stringTranslation->translate('Shop URLs'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['systempay_urls_to_shop']['systempay_shop_url'] = [
      '#type' => 'textfield',
      '#title' => $this->stringTranslation->translate('Shop main URL'),
      '#description' => $this->stringTranslation->translate('The Shop URL appears on payment page and in mails'),
      '#default_value' => $this->configuration['systempay_shop_url'] ?? '',
    ];
    $form['systempay_urls_to_shop']['systempay_url_return'] = [
      '#type' => 'textfield',
      '#title' => $this->stringTranslation->translate('Go back URL'),
      '#description' => $this->stringTranslation->translate('URL to redirect user after click on "Go back" button'),
      '#default_value' => $this->configuration['systempay_url_return'] ?? '',
    ];
    $form['systempay_urls_to_shop']['systempay_url_error'] = [
      '#type' => 'textfield',
      '#title' => $this->stringTranslation->translate('Error URL'),
      '#description' => $this->stringTranslation->translate('URL to redirect user after a intern error'),
      '#default_value' => $this->configuration['systempay_url_error'] ?? '',
    ];
    $form['systempay_urls_to_shop']['systempay_url_success'] = [
      '#type' => 'textfield',
      '#title' => $this->stringTranslation->translate('Success URL'),
      '#description' => $this->stringTranslation->translate('URL to redirect after a successful payment'),
      '#default_value' => $this->configuration['systempay_url_success'] ?? '',
    ];
    $form['systempay_urls_to_shop']['systempay_url_cancel'] = [
      '#type' => 'textfield',
      '#title' => $this->stringTranslation->translate('Cancel URL'),
      '#description' => $this->stringTranslation->translate('URL to redirect after a click on "Cancel"'),
      '#default_value' => $this->configuration['systempay_url_cancel'] ?? '',
    ];
    $form['systempay_urls_to_shop']['systempay_url_referral'] = [
      '#type' => 'textfield',
      '#title' => $this->stringTranslation->translate('Referral URL'),
      '#description' => $this->stringTranslation->translate('URL to redirect after an error code 02 "contacter the card issuer"'),
      '#default_value' => $this->configuration['systempay_url_referral'] ?? '',
    ];
    $form['systempay_urls_to_shop']['systempay_url_refused'] = [
      '#type' => 'textfield',
      '#title' => $this->stringTranslation->translate('Refusal URL'),
      '#description' => $this->stringTranslation->translate('URL to redirect after any other case than the refusal of authorization code 02'),
      '#default_value' => $this->configuration['systempay_url_refused'] ?? '',
    ];

    // ==========================> Fieldset : Return To Shop.
    $form['systempay_return_to_shop'] = [
      '#type' => 'fieldset',
      '#title' => $this->stringTranslation->translate('Return to shop'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['systempay_return_to_shop']['systempay_redirect_enabled'] = [
      '#type' => 'radios',
      '#title' => $this->stringTranslation->translate('Automatic forward'),
      '#description' => $this->stringTranslation->translate('If enabled, the client is automatically forwarded to your site at the end of the payment process'),
      '#options' => $systempayHelper->getAvailableAutomaticForwardModes(),
      '#default_value' => $this->configuration['systempay_redirect_enabled'] ?? '',
    ];
    $form['systempay_return_to_shop']['systempay_redirect_success_timeout'] = [
      '#type' => 'textfield',
      '#size' => '20',
      '#title' => $this->stringTranslation->translate('Success forward timeout'),
      '#description' => $this->stringTranslation->translate('Time in seconds (0-300) before the client is automatically forwarded to your site when the payment was successful'),
      '#default_value' => $this->configuration['systempay_redirect_success_timeout'] ?? '5',
    ];
    $form['systempay_return_to_shop']['systempay_redirect_success_message'] = [
      '#type' => 'textfield',
      '#title' => $this->stringTranslation->translate('Success forward message'),
      '#description' => $this->stringTranslation->translate('Message posted on the payment platform before forwarding when the payment was successful'),
      '#default_value' => $this->configuration['systempay_redirect_success_message'] ?? 'Redirection vers la boutique dans quelques instants...',
    ];
    $form['systempay_return_to_shop']['systempay_redirect_error_timeout'] = [
      '#type' => 'textfield',
      '#size' => '20',
      '#title' => $this->stringTranslation->translate('Failure forward timeout'),
      '#description' => $this->stringTranslation->translate('Time in seconds (0-300) before the client is automatically forwarded to your site when the payment failed'),
      '#default_value' => $this->configuration['systempay_redirect_error_timeout'] ?? '5',
    ];
    $form['systempay_return_to_shop']['systempay_redirect_error_message'] = [
      '#type' => 'textfield',
      '#title' => $this->stringTranslation->translate('Failure forward message'),
      '#description' => $this->stringTranslation->translate('Message posted on the payment platform before forwarding when the payment failed'),
      '#default_value' => $this->configuration['systempay_redirect_error_message'] ?? 'Redirection vers la boutique dans quelques instants...',
    ];
    $form['systempay_return_to_shop']['systempay_return_mode'] = [
      '#type' => 'select',
      '#title' => $this->stringTranslation->translate('Return mode'),
      '#description' => $this->stringTranslation->translate('Method that will be used for transmitting the payment result from the payment gateway to your store'),
      '#options' => $systempayHelper->getAvailableReturnModes(),
      '#default_value' => $this->configuration['systempay_return_mode'] ?? 'GET',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\plugin\PluginType\PluginTypeManagerInterface $plugin_type_manager */
    $plugin_type_manager = $container->get('plugin.plugin_type_manager');

    return new static($configuration, $plugin_id, $plugin_definition, $container->get('string_translation'), $container->get('module_handler'), $container->get('plugin.manager.plugin.plugin_selector'), $plugin_type_manager->getPluginType('payment_status'));
  }

  /**
   * Returns the brand label.
   *
   * @return string
   *   The brand label.
   */
  public function getBrandLabel() {
    return 'SystemPay Standard';
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $systempay_module_param = NestedArray::getValue($values, $form['systempay_module_param']['#parents'])
      + NestedArray::getValue($values, $form['systempay_payment_page']['#parents'])
      + NestedArray::getValue($values, $form['systempay_urls_to_shop']['#parents'])
      + NestedArray::getValue($values, $form['systempay_return_to_shop']['#parents']);

    foreach ($systempay_module_param as $key => $value) {
      $this->configuration[$key] = $value;
    }
  }

}
