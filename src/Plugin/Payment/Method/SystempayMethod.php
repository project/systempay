<?php

declare(strict_types = 1);

namespace Drupal\systempay\Plugin\Payment\Method;

use Drupal\Core\Url;
use Drupal\payment\OperationResult;
use Drupal\payment\Plugin\Payment\Method\PaymentMethodBase;
use Drupal\payment\Response\Response;

/**
 * SystemPay payment method.
 *
 * @PaymentMethod(
 *     active=TRUE,
 *     id="systempay_payment",
 *     label=@Translation("Systempay"),
 *     module="systempay",
 *     deriver="\Drupal\systempay\Plugin\Payment\Method\SystempayDeriver",
 *     operations_provider="\Drupal\systempay\Plugin\Payment\Method\SystempayOperationsProvider",
 * )
 */
class SystempayMethod extends PaymentMethodBase {

  /**
   * {@inheritdoc}
   */
  public function getPaymentExecutionResult() {

    /** @var \Drupal\payment\Entity\PaymentInterface $payment */
    $payment = $this->getPayment();

    // Payment ID is in the URL but the access to this route is checked.
    $url = Url::fromRoute('systempay.systempay_init_form', ['payment' => $payment->id()]);
    $response = new Response($url);

    return new OperationResult($response);
  }

  /**
   * Returns the supported currencies.
   *
   * @return \Drupal\payment\Plugin\Payment\Method\SupportedCurrencyInterface[]|true
   *   Return TRUE to allow all currencies and amounts.
   */
  protected function getSupportedCurrencies() {
    // @todo Maybe fix this?
    return TRUE;
  }

}
