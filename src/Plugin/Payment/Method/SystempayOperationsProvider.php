<?php

declare(strict_types = 1);

namespace Drupal\systempay\Plugin\Payment\Method;

use Drupal\payment\Plugin\Payment\Method\BasicOperationsProvider;

/**
 * Provider for Systempay operations.
 */
class SystempayOperationsProvider extends BasicOperationsProvider {

  /**
   * {@inheritdoc}
   */
  protected function getPaymentMethodConfiguration($plugin_id) {
    $entity_id = explode(':', $plugin_id)[1];

    return $this->paymentMethodConfigurationStorage->load($entity_id);
  }

}
