<?php

declare(strict_types = 1);

namespace Drupal\systempay\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\payment\Entity\Payment as PaymentEntity;
use Drupal\payment\Payment;
use Drupal\systempay\Libs\SystempayHelper;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Controller for Systempay webhook.
 */
class Webhook extends ControllerBase {

  /**
   * The current payment entity.
   *
   * @var \Drupal\payment\Entity\PaymentInterface
   */
  private $payment;

  /**
   * The request array.
   *
   * @var array
   */
  private $postData;

  /**
   * The helper class containing all methods.
   *
   * @var \Drupal\systempay\Libs\SystempayHelper
   */
  private $systempayHelper;

  /**
   * Webhook constructor.
   */
  public function __construct() {
    // First, get request info.
    // @todo Use dependency injection here.
    $this->postData = \Drupal::request()->request->all();

    // Get the entity ID and load it.
    if (!$this->payment = PaymentEntity::load($this->postData[SystempayHelper::getUselessFieldValue()])) {
      throw new HttpException(404, 'Not Found');
    }

    // Now we can grab the plugin that has been used and its config.
    $plugin_definition = $this->payment->getPaymentMethod()->getPluginDefinition();

    // Check first if it's the good plugin.
    if ($plugin_definition['provider'] !== 'systempay') {
      throw new HttpException(500, 'Wrong plugin has been used for Systempay.');
    }

    // And now we can build the helper class with the right configuration.
    $this->systempayHelper = new SystempayHelper($plugin_definition);
  }

  /**
   * Custom access provider.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   True if authorized.
   */
  public function access() {
    return AccessResult::allowedIf($this->systempayHelper->checkDataIntegrity($this->postData));
  }

  /**
   * Execute the webhook, signature has been already verified.
   */
  public function execute() {
    if (!$paymentStatusPluginId = $this->systempayHelper->getStatusIdFromCode($this->postData['vads_result'])) {
      throw new HttpException(500, 'Invalid code');
    }

    if (!$paymentStatus = Payment::statusManager()->createInstance($paymentStatusPluginId)) {
      throw new HttpException(500, 'Invalid plugin id');
    }

    // Update payment status and save entity.
    $this->payment->setPaymentStatus($paymentStatus);
    // This save can be hooked by the payment type to implement callback.
    $this->payment->save();

    // Return a 200 code for SystemPay back office.
    return new Response('Action successfully registered.', 200);
  }

}
