<?php

declare(strict_types = 1);

namespace Drupal\systempay\Libs;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\payment\Entity\PaymentInterface;

/**
 * Provide common methods.
 */
class SystempayHelper {
  use StringTranslationTrait;

  /**
   * The configuration array.
   *
   * Because here we are using a plugin, that's why the config is an
   * array instead of an object.
   *
   * @var array
   */
  private $config;

  /**
   * SystempayHelper constructor.
   *
   * @param array $config
   *   The configuration for the plugin.
   */
  public function __construct(array $config) {
    $this->config = $config;
  }

  /**
   * Add useless zeros so the ID has 6 numbers in any case.
   *
   * @param string $value
   *   Initial value.
   *
   * @return string
   *   Six numbers conversion.
   */
  public function addUselessZeros($value) {
    return str_pad($value, 6, '0', \STR_PAD_LEFT);
  }

  /**
   * Check if transaction succeeds.
   *
   * - Grab fields from post data.
   * - Calculate signature.
   * - Compare signatures.
   * - Analyse the notification.
   * - Check if paiement is valid or not.
   *
   * @param array $systempay_return
   *   The array sent by System API.
   *
   * @return bool
   *   True if paiement succeeded.
   */
  public function checkDataIntegrity(array $systempay_return) {
    $signature = $this->generateSignature($systempay_return);

    return isset($systempay_return['signature']) && $signature === $systempay_return['signature'];
  }

  /**
   * Given the vads array, make the render array for the form.
   *
   * @param mixed $vads_array
   *   Array with all the VADS fields.
   *
   * @return mixed
   *   The render array of the form.
   */
  public function createFormFromArray($vads_array) {
    $form = [];
    // Change the target of the form to systempay.
    $form['#action'] = $vads_array['link'];
    $form['#attributes']['class'][] = 'systempay_container';
    $form['#cache']['max-age'] = 0;

    // Add all vads fields.
    foreach ($vads_array['fields'] as $key => $value) {
      $form[$key] = [
        '#type' => 'hidden',
        '#default_value' => $value,
      ];
    }
    // Add signature.
    $form['signature'] = [
      '#type' => 'hidden',
      '#default_value' => $vads_array['signature'],
    ];
    // Add the template.
    $form['animation'] = [
      '#theme' => 'systempay_init',
    ];
    // Add a submit button.
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('If the redirection is not working, click here.'),
      '#attributes' => [
        'class' => [
          'btn',
          'btn-assertive',
          'systempay_submit_button',
        ],
      ],
    ];

    return $form;
  }

  /**
   * Given a payment, returns the formatted array for display.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   The transaction.
   *
   * @return mixed
   *   The render array.
   */
  public function createVadsArray(PaymentInterface $payment) {
    // Make date in the systempay format (ex : 20170303112845).
    $timestamp = DrupalDateTime::createFromTimestamp($payment->get('created')->getValue()[0]['value']);
    $systempay_formatted_timestamp = $timestamp->format('YmdHis');

    /*
     * Make the transaction reference.
     * By default, it's the payment entity id, but it can be hooked by
     * your custom payment type in order to put whatever you want in it.
     *
     * @see systempay.api.php
     */
    $payment_transaction_reference = $payment->id();

    // Import config.
    $config_array = $this->config;
    // For this page, cf page 40 of documentation.
    $output['link'] = $config_array['systempay_platform_url'];
    $output['fields']['vads_action_mode'] = 'INTERACTIVE';
    // Multiply the amount by 100 (Systempay requirement).
    $output['fields']['vads_amount'] = $this->addUselessZeros($payment->getAmount() * 100);
    $output['fields']['vads_capture_delay'] = $config_array['systempay_capture_delay'];
    $output['fields']['vads_ctx_mode'] = $config_array['systempay_ctx_mode'];
    $output['fields']['vads_currency'] = $payment->getCurrency()->getCurrencyNumber();
    $output['fields']['vads_language'] = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $output['fields']['vads_page_action'] = 'PAYMENT';
    $output['fields']['vads_order_id'] = $payment_transaction_reference;
    $output['fields']['vads_payment_config'] = 'SINGLE';
    $output['fields'][self::getUselessFieldValue()] = $payment->id();
    $output['fields']['vads_site_id'] = $config_array['systempay_site_id'];
    $output['fields']['vads_trans_date'] = $systempay_formatted_timestamp;
    $output['fields']['vads_trans_id'] = $this->generateTransactionId();
    $output['fields']['vads_validation_mode'] = $config_array['systempay_validation_mode'];
    $output['fields']['vads_version'] = 'V2';

    // Give the possibility to alter the array before calculating signature.
    $module_handler = \Drupal::moduleHandler();
    // We don't want the payment to be altered, so we clone it.
    $payment_reference = clone $payment;
    $module_handler->alter('systempay_vads_data', $output, $payment_reference);

    $output['signature'] = $this->generateSignature($output['fields']);

    return $output;
  }

  /**
   * Generate the signature according to systempay documentation.
   *
   * @param array $fields
   *   The associative array containing parameters.
   *
   * @return string
   *   The signature.
   */
  public function generateSignature(array $fields) {
    $concat = '';

    // First, order fields in alphabetic order.
    ksort($fields);

    foreach ($fields as $key => $value) {
      // Grab only fields starting with vads.
      if (substr($key, 0, 5) === 'vads_') {
        $concat .= $value . '+';
      }
    }

    if ($fields['vads_ctx_mode'] === 'PRODUCTION') {
      $concat .= $this->config['systempay_key_prod'];
    }
    else {
      $concat .= $this->config['systempay_key_test'];
    }

    return sha1($concat);
  }

  /**
   * Generate an ID for the transaction.
   *
   * Systempay has some weird rules for the ID :
   * it should be between 100000 and 899999 and reset
   * everyday. This function provides this ID based on
   * State API.
   *
   * @todo what if we have several instances of this plugin?
   *
   * @return int
   *   Transaction ID.
   */
  public function generateTransactionId() {
    $systempay_min = 100000;
    $today = (new DrupalDateTime('now', DateTimeItemInterface::STORAGE_TIMEZONE))->format(DateTimeItemInterface::DATE_STORAGE_FORMAT);

    // Get last transaction infos.
    $last_updated_date = \Drupal::state()->get('systempay.last_updated_date');
    $current_transaction_id = \Drupal::state()->get('systempay.current_transaction_id');

    // If nothing is registered, create the first entry.
    if ($current_transaction_id === NULL || $last_updated_date === NULL) {
      // If they are not defined, init them.
      $last_updated_date = $today;
      $current_transaction_id = $systempay_min;
      \Drupal::state()->setMultiple([
        'systempay.last_updated_date' => $today,
        'systempay.current_transaction_id' => $systempay_min,
      ]);
    }

    // If last date is today, increment id. If not, reset and change date.
    if ($last_updated_date === $today) {
      $id = $current_transaction_id + 1;
    }
    else {
      $id = $systempay_min;
      \Drupal::state()->set('systempay.last_updated_date', $today);
    }

    // Write ID in state API for the next call.
    \Drupal::state()->set('systempay.current_transaction_id', $id);

    return $id;
  }

  /**
   * Generates an array containing values for property "Automatic Forward".
   *
   * @return array
   *   The possible values.
   */
  public function getAvailableAutomaticForwardModes() {
    return [
      '0' => $this->t('No'),
      '1' => $this->t('Yes'),
    ];
  }

  /**
   * Return an array of languages.
   *
   * @param \Drupal\Core\Language\Language[] $languages
   *   Available languages.
   *
   * @return array
   *   Key Value renderable array.
   */
  public function getAvailableLanguages(array $languages) {
    $language_array = [];

    /** @var \Drupal\Core\Language\Language $language */
    foreach ($languages as $language) {
      $language_array[$language->getId()] = $language->getName();
    }

    return $language_array;
  }

  /**
   * Generates an array containing possible values for property "Return Mode".
   *
   * @return array
   *   The possible values.
   */
  public function getAvailableReturnModes() {
    return ['GET', 'POST'];
  }

  /**
   * Generates an array containing values for property "Validation Mode".
   *
   * @return array
   *   The possible values.
   */
  public function getAvailableValidationModes() {
    return [
      '' => $this->t('Back-office configuration'),
      '0' => $this->t('Automatic'),
      '1' => $this->t('Manual'),
    ];
  }

  /**
   * From the systempay return code, returns the corresponding Payment status.
   *
   * @param int $code
   *   Systempay code for the status.
   *
   * @return string|null
   *   The plugin id.
   */
  public static function getStatusIdFromCode($code) {
    $output = NULL;

    /*
     * Listing all systempay cases, and returning the plugin id of Payment.
     * The comments are in French to match the labels in the documentation.
     * @see Documentation v3.10 page 138.
     */
    switch ($code) {
      // Action réalisée avec succès.
      case '00':
        $output = 'payment_authorized';
        break;

      // Le marchand doit contacter la banque du porteur. Déprécié.
      case '02':
        $output = 'payment_failed';
        break;

      // Action refusée.
      case '05':
        $output = 'payment_authorization_failed';
        break;

      // Annulation de l'acheteur.
      case '17':
        $output = 'payment_cancelled';
        break;

      // Erreur de format de la requête.
      case '30':
        $output = 'payment_failed';
        break;

      // Erreur technique.
      case '96':
        $output = 'payment_failed';
        break;

      // Expiré (custom).
      case '98':
        $output = 'payment_expired';
        break;
    }

    return $output;
  }

  /**
   * Returns an array of card types accepted by the Systempay payment platform.
   *
   * @return array[string]string
   *   Card types.
   */
  public function getSupportedCardTypes() {
    return [
      'CB' => 'CB',
      'VISA' => 'Visa',
      'VISA_ELECTRON' => 'Visa Electron',
      'MASTERCARD' => 'Mastercard',
      'MAESTRO' => 'Maestro',
      'AMEX' => 'American Express',
      'E-CARTEBLEUE' => 'E-Carte bleue',
    ];
  }

  /**
   * This is actually a hack.
   *
   * Systempay doesn't allow us to have a field storing the entity ID,
   * so I just took a useless field to store it. I also exposed it with
   * a getter.
   *
   * @return string
   *   The value of the useless field.
   */
  public static function getUselessFieldValue() {
    return 'vads_ship_to_district';
  }

}
