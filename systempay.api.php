<?php

/**
 * @file
 * Hooks provided by the Systempay module.
 */

declare(strict_types = 1);

use Drupal\payment\Entity\PaymentInterface;

/**
 * Provides the possibility to alter the transaction reference.
 *
 * If not hooked, the reference displayed in Systempay backoffice
 * will be the entity id.
 *
 * @param array $vadsArray
 *   The VADS array before signature calculation.
 * @param \Drupal\payment\Entity\PaymentInterface $payment
 *   Payment entity.
 */
function hook_systempay_vads_data_alter(array &$vadsArray, PaymentInterface $payment) {
  /*
   * Example of implementation : your custom payment type provides
   * a reference field that you want to use as the reference.
   */
  $vadsArray['fields']['vads_order_id'] = $payment->get('field_transaction_reference');
}
