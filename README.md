# Introduction

This module provides an interface between 
[Payment for Drupal 8](https://www.drupal.org/project/payment) 
and SystemPay, the payment method for Caisse d'Épargne and 
other french banks.

It's based on the last version 
[Systempay official doc](https://systempay.cyberpluspaiement.com/html/Doc/Payment_Form/Guide_d_implementation_formulaire_paiement_Systempay_v3.10.pdf) 
(or at least it was in May 2017).

# Requirements

- Payment module (https://drupal.org/project/payment)
- You have to write your own payment type, refer to
[Payment official documentation](https://www.drupal.org/node/1807610)

# Installation
Install as you would normally install a contributed Drupal 
module.
 
# Configuration
Go to `/admin/config/services/payment/method` and add a new 
Payment method configuration, chose Systempay, then fill the 
form with informations provided by Systempay. 

# Todo
Lots of functions covered by the Systempay API are not 
implemented in this module, like the possibility to add 
several products, extra parameters that can be provided to
the Systempay endpoint… due to a lack of time. 
But the hardest work has already been done and the module 
works perfectly fine on simple payment operations.

Unit tests seems to be a good idea too.
